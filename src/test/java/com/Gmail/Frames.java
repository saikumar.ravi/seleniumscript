package com.Gmail;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Frames {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get("https://www.selenium.dev/selenium/docs/api/java/index.html?overview-summary.html");
        driver.switchTo().frame("packageListFrame");
		driver.findElement(By.partialLinkText("org.openqa.selenium")).click();
		driver.navigate().refresh();
		Thread.sleep(4000);
		
		
		driver.switchTo().frame("packageFrame");
		driver.findElement(By.partialLinkText("AbstractAnnotations")).click();
		driver.navigate().refresh();
		Thread.sleep(4000);


		
		driver.switchTo().frame("classFrame");
		driver.findElement(By.partialLinkText("org.openqa.selenium.chrome")).click();
		driver.navigate().refresh();
		Thread.sleep(4000);


        
	}

}
